//
//  GetFoodViewController.swift
//  Community-App
//
//  Created by Katelyn Ashok on 2/26/22.
//

import UIKit
public var p_order : [String] = []
public var p_quants : [String] = []
public var p_times : [String] = []

class GetFoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //variables
    var orders: [String] = []
    var quants: [String] = []
    var times: [String] = []
    
    @IBOutlet weak var foodText: UITextField!
    @IBOutlet weak var foodQuantity: UITextField!
    @IBOutlet weak var foodTimeFrame: UITextField!
    @IBOutlet weak var foodOrderTable: UITableView!
    let textCellIdentifier = "TextCell"
    let submitSegueIdentifier = "getFoodConfirmationSegue"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        orders = []
        quants = []
        times = []

        // Do any additional setup after loading the view.
        foodOrderTable.delegate = self
        foodOrderTable.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        orders = []
        quants = []
        times = []
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = foodOrderTable.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)
        let row = indexPath.row
        print("\(orders[row]), \(quants[row]), \(times[row])")
        cell.textLabel?.text = "\(orders[row]), \(quants[row]), \(times[row])"
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        let row = indexPath.row
////        print(orders[row])
//    }

    @IBAction func orderButtonPressed(_ sender: Any) {
        guard let name = foodText.text else { return}
        guard let q = foodQuantity.text else { return}
        guard let time = foodTimeFrame.text else { return}
        orders.append(name)
        quants.append(q)
        times.append(time)
        
        p_order.append(name)
        p_quants.append(q)
        p_times.append(time)
         
        foodOrderTable.reloadData()
        
        //clear textboxes
        foodText.text? = ""
        foodQuantity.text? = ""
        foodTimeFrame.text? = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == submitSegueIdentifier,
           let destination = segue.destination as? GetFoodConfirmationViewController{
           destination.orders = orders
            destination.quants = quants
            destination.times = times
            destination.caller = self
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
