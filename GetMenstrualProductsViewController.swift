//
//  GetMenstrualProductsViewController.swift
//  Community-App
//
//  Created by Katelyn Ashok on 2/26/22.
//

import UIKit

//var products:String[]
//var numProducts:String[]

class GetMenstrualProductsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    //vars
    @IBOutlet weak var tableView: UITableView!
    let textCellIdentifier = "TextCell"
    var products:[String] = ["pads", "tampons", "menstrual cups", "underwear", "menstrual discs"]
    //var currentChosenProducts:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)
        let row = indexPath.row
        cell.textLabel?.text = products[row]
        cell.detailTextLabel?.text = "Quantity = 0"
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let currentQ:String = cell?.detailTextLabel?.text ?? "0"
        var quantity: Int = Int(currentQ) ?? 0
        cell?.detailTextLabel?.text = String(quantity)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
