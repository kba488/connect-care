//
//  giveFoodViewController.swift
//  Community-App
//
//  Created by Katelyn Ashok on 2/26/22.
//

import UIKit

class giveFoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //vars
    @IBOutlet weak var foodRequestTable: UITableView!
    let textCellIdentifier = "TextCell"
    let getFoodSegueIdentifier = "getFoodSegueIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        foodRequestTable.delegate = self
        foodRequestTable.dataSource = self
        print(p_order)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = foodRequestTable.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)
        let row = indexPath.row
        print("\(p_order[row]), \(p_quants[row]), \(p_times[row])")
        cell.textLabel?.text = "\(p_order[row]), \(p_quants[row]), \(p_times[row])"
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return p_order.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        foodRequestTable.deselectRow(at: indexPath, animated: true)
//        let row = indexPath.row

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == getFoodSegueIdentifier,
           let destination = segue.destination as? SelectedFoodRequestViewController,
           let requestIndex = foodRequestTable.indexPathForSelectedRow?.row {
            destination.order = "\(p_order[requestIndex]), \(p_quants[requestIndex]), \(p_times[requestIndex])"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
