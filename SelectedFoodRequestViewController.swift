//
//  SelectedFoodRequestViewController.swift
//  Community-App
//
//  Created by Katelyn Ashok on 2/27/22.
//

import UIKit

class SelectedFoodRequestViewController: UIViewController {

    //vars
    var order:String = ""
    @IBOutlet weak var request: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        request.text = order
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
