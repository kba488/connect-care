//
//  GetFoodConfirmationViewController.swift
//  Community-App
//
//  Created by Katelyn Ashok on 2/27/22.
//

import UIKit

class GetFoodConfirmationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    //vars
    @IBOutlet weak var tableView: UITableView!
    let textCellIdentifier = "TextCell"
    var orders: [String] = []
    var quants: [String] = []
    var times: [String] = []
    var caller:UIViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)
        let row = indexPath.row
        cell.textLabel?.text = "\(orders[row]), \(quants[row]), \(times[row])"
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }

    
    @IBAction func backHomePress(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        //self.dismiss
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
